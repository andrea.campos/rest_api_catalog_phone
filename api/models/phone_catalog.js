import mongoose from "mongoose";

const phoneCatalogSchema = mongoose.Schema({
  id: Number,
  name: String,
  manufacturer: String,
  description: String,
  color: String,
  price: Number,
  image: String,
  screen: String,
  processor: String,
  ram: Number,
});

export default mongoose.model('phone_catalog', phoneCatalogSchema);