import express from "express";
import mongoose from "mongoose";
import phone_catalog from "./api/models/phone_catalog.js";
import cors from 'cors';

const app = express();
const port = 5500;

app.use(cors())

// string generate with mongoDB
const mongoURL =
  "mongodb+srv://acamposcar:phones123@cluster0.irmwb.mongodb.net/phones?retryWrites=true&w=majority";

// get method (use postman)
app.get("/api/phone", (req, res) => {
  phone_catalog.find({}, (err, data) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.status(200).send(data);
    }
  });
});

// middleware to recognoise json
app.use(express.json());

// post method (postman, mongoDB)
app.post("/api/phone", (req, res) => {
  let phoneData = req.body;
  let mongoRecord = [];

  phoneData.forEach((phone) => {
    mongoRecord.push({
      id: phone.id,
      name: phone.name,
      manufacturer: phone.manufacturer,
      description: phone.description,
      color: phone.color,
      price: phone.price,
      image: phone.image,
      screen: phone.screen,
      processor: phone.processor,
      ram: phone.ram,
    });
  });
  phone_catalog.create(mongoRecord, (err, records) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.status(200).send(records);
    }
  });
});

// delete or filter database (in this case, delete repetead records)
app.delete("/api/phone", (req, res) => {
  phone_catalog.deleteMany({}, (err) => {
    res.status(500).send(err);
  });
});

// conect server with mongoDB (databse)
mongoose.connect(mongoURL, { useNewUrlParser: true, useUnifiedTopology: true });

// listening port 5500
app.listen(port, () => {
  console.log(`Server is listening at http://localhost:${port}`);
});



